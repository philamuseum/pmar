#' Date to FY
#' 
#' takes a vector of dates and converts it to the fiscal year
#' assumes fiscal year starts in July for simplicity
#' 
#' @return returns fiscal year as a numberic variable


date_fy <- function(date) {
  year <- lubridate::year(date)
  year <- ifelse(lubridate::month(date) %in% 7:12, year + 1, year)
  year
}